(function() {
  angular.module('appDirectives', [])
    .directive('navheader', function(){
      return {
        restrict: 'E',
        templateUrl: 'templates/navheader.html',
        controller: 'navbarController'
      };
    });
})();